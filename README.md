# boids

This is a simulation of "boids". Basicaly a model of how large groups of entities move together like fish in a pond or swarms of birds.

They all individualy choose where to go yet they seem to move as one. This happens becease they mimic the movement of their neighbours.

## Setup

To install dependencies run `npm install`.

To build run `npm run build`

If you want to develop run `npm run dev`, this will make webpack watch the files you edit en keep updating your compiled code.

> **Feel free to suggest extra features**

## Code

I designed the in a way that you can easily make and add rules.

Here I make a builder with some pre-defined rules.

```js
const builder = new BoidBuilder()
    .addSpeedRule(rules.maxSpeed(5))
    .addLocationRule(rules.keepInBounds(worldSize))
    .addBoidRule(rules.separation(boids, 15, 0.3))
    .addBoidRule(rules.alignment(boids, 20, 0.01))
    .addBoidRule(rules.cohesion(boids, 30, 0.03));
```

> Note: The order in which you add the rules matters (They get applied in order)

Here I instantiate a few instances boid.

```js
// this makes a random boid within a `worldSize` with a speed of 1
const boid: Boid = builder.buildRandom(worldSize, 1);

// here we choose our selfs where the boid is
const boid: Boid = builder.build(
    // location
    new Coordinate(500, 500),
    // speed
    new Coordinate(1, 2)
);
```

## Sources

* [Some article about Boids](https://www.red3d.com/cwr/boids/)
