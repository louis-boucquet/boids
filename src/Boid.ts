import Movable from "./Movable";
import Coordinate from "./Coordinate";
import { BoidRule, LocationRule, SpeedRule } from "./RuleTypes";

export default class Boid extends Movable {
    constructor(
        location: Coordinate,
        locationRules: Array<LocationRule>,
        speed: Coordinate,
        speedRules: Array<SpeedRule>,
        private boidRules: Array<BoidRule>
    ) {
        super(location, locationRules, speed, speedRules);
    }

    public update(): void {
        // calculate the acceleration
        const acceleration = this.boidRules
            .map(rule => rule(this))
            .reduce(Coordinate.add, Coordinate.zero());

        // console.log(this.boidRules);

        // apply acceleration and movement
        super.addSpeed(acceleration);
        super.update();
    }
}
