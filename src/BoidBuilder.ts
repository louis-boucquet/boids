import { BoidRule, LocationRule, SpeedRule } from "./RuleTypes";
import Boid from "./Boid";
import Coordinate from "./Coordinate";

export default class BoidBuilder {

    private boidRules: Array<BoidRule> = new Array();
    private locationRules: Array<LocationRule> = new Array();
    private speedRules: Array<SpeedRule> = new Array();

    constructor() { }

    public addBoidRule(rule: BoidRule): BoidBuilder {
        this.boidRules.push(rule);

        return this;
    }

    public addLocationRule(rule: LocationRule): BoidBuilder {
        this.locationRules.push(rule);

        return this;
    }

    public addSpeedRule(rule: SpeedRule): BoidBuilder {
        this.speedRules.push(rule);

        return this;
    }

    public build(location: Coordinate, speed: Coordinate): Boid {
        return new Boid(
            location,
            this.locationRules,
            speed,
            this.speedRules,
            this.boidRules
        );
    }

    public buildRandom(bounds: Coordinate, speed: number): Boid {
        return new Boid(
            Coordinate.randomInBounds(bounds),
            this.locationRules,
            Coordinate.randomFromZero(speed),
            this.speedRules,
            this.boidRules
        );
    }
}
