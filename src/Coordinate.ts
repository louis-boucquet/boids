export default class Coordinate {
    constructor(
        public x: number,
        public y: number
    ) {
    }

    static zero() {
        return new Coordinate(0, 0)
    }

    static add(c1: Coordinate, c2: Coordinate) {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y)
    }

    static sub(c1: Coordinate, c2: Coordinate) {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y)
    }

    static multiplyNumber(c: Coordinate, n: number) {
        return new Coordinate(c.x * n, c.y * n)
    }

    static random(start: Coordinate, end: Coordinate) {
        const size = Coordinate.sub(end, start)
        return new Coordinate(start.x + Math.random() * size.x, start.y + Math.random() * size.y)
    }

    static randomInBounds(bounds: Coordinate) {
        return new Coordinate(Math.random() * bounds.x, Math.random() * bounds.y)
    }

    static randomFromZero(dist: number) {
        const lowerBound = new Coordinate(-dist, -dist)
        const upperBound = new Coordinate(dist, dist)

        return Coordinate.random(lowerBound, upperBound)
    }

    static dist(c1: Coordinate, c2: Coordinate) {
        return Math.sqrt(Math.pow(c2.x - c1.x, 2) + Math.pow(c2.y - c1.y, 2))
    }

    static size(c: Coordinate) {
        return Coordinate.dist(c, Coordinate.zero())
    }

    static normalize(c: Coordinate, normalLength: number = 1) {
        const currLength = Coordinate.size(c)
        if (currLength === 0)
            return c
        else if (normalLength === 0)
            return Coordinate.zero()
        return new Coordinate(c.x / currLength * normalLength, c.y / currLength * normalLength)
    }

    static keepInBounds(coordinate: Coordinate, bounds: Coordinate): Coordinate {
        // create a copy that we can modify
        coordinate = Object.assign({}, coordinate);

        while (coordinate.x > bounds.x)
            coordinate.x -= bounds.x;

        while (coordinate.x < 0)
            coordinate.x += bounds.x;

        while (coordinate.y > bounds.y)
            coordinate.y -= bounds.y;

        while (coordinate.y < 0)
            coordinate.y += bounds.y;

        return coordinate;
    }

    static getDirection(direction: string): Coordinate {
        switch (direction) {
            case "left":
                return { x: -1, y: 0 }
            case "right":
                return { x: 1, y: 0 }
            case "up":
                return { x: 0, y: -1 }
            case "down":
                return { x: 0, y: 1 }
            default:
                return { x: 0, y: 0 }
        }
    }
}
