import Coordinate from "./Coordinate";
import { LocationRule } from "./RuleTypes";

export default class Entity {
    constructor(
        public location: Coordinate,
        private locationRules: Array<LocationRule>
    ) { }

    render(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.arc(this.location.x, this.location.y, 2, 0, 2 * Math.PI);
        ctx.fill();
    }

    update() {
        this.location = this.locationRules
            .reduce((loc, rule) => rule(loc), this.location)
    }

    keepInBounds(bounds: Coordinate) {
        this.location = Coordinate.keepInBounds(this.location, bounds);
    }
}
