import Entity from "./Entity";
import Coordinate from "./Coordinate";
import { LocationRule, SpeedRule } from "./RuleTypes";

export default class Movable extends Entity {
    constructor(
        location: Coordinate,
        locationRules: Array<LocationRule>,
        public speed: Coordinate,
        private speedRules: Array<SpeedRule>
    ) {
        super(location, locationRules);
    }

    public update(): void {
        this.speed = this.speedRules
            .reduce((speed, rule) => rule(speed), this.speed)

        this.location = Coordinate.add(this.location, this.speed);

        super.update();
    }

    public render(ctx: CanvasRenderingContext2D): void {
        super.render(ctx)

        const size = Math.sqrt(Coordinate.size(this.speed) * 0.5) * 5;

        const sizeCoordinate = Coordinate.normalize(this.speed, size);

        const endPoint = Coordinate.add(this.location, sizeCoordinate);

        ctx.lineWidth = 1.5;
        ctx.beginPath();
        ctx.moveTo(this.location.x, this.location.y);
        ctx.lineTo(endPoint.x, endPoint.y);
        ctx.stroke();
    }

    public addSpeed(acceleration: Coordinate): void {
        this.speed = Coordinate.add(this.speed, acceleration);
    }
}
