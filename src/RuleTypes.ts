import Coordinate from "./Coordinate";
import Boid from "./Boid";

export interface BoidRule {
    (boid: Boid): Coordinate;
}

export interface LocationRule {
    (location: Coordinate): Coordinate;
}

export interface SpeedRule {
    (speed: Coordinate): Coordinate;
}
