import BoidBuilder from "./BoidBuilder";
import Coordinate from "./Coordinate";
import Boid from "./Boid";
import rules from "./rules";

// Get drawing context

const canvas = document.createElement("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

document.body.appendChild(canvas);

const ctx: CanvasRenderingContext2D = <CanvasRenderingContext2D>canvas.getContext("2d");

const worldSize = new Coordinate(window.innerWidth, window.innerHeight);

// instantiate a few boids

const boids: Array<Boid> = new Array();

const builder = new BoidBuilder()
    // .addSpeedRule(rules.acceleration(new Coordinate(0.01, 0.01)))
    .addSpeedRule(rules.maxSpeed(5))
    .addLocationRule(rules.keepInBounds(worldSize))
    .addBoidRule(rules.separation(boids, 15, 0.3))
    .addBoidRule(rules.alignment(boids, 20, 0.01))
    .addBoidRule(rules.cohesion(boids, 30, 0.03));

for (let i = 0; i < 500; i++) {
    const boid: Boid = builder.buildRandom(worldSize, 1);

    boids.push(boid);
}

// Start update cycle

update();

function update() {
    ctx.clearRect(0, 0, worldSize.x, worldSize.y)

    for (const boid of boids) {
        boid.update();

        boid.render(ctx);
    }
    // setTimeout(update, 1000)

    requestAnimationFrame(update);
}
