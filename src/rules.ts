import Coordinate from "./Coordinate";
import { BoidRule, SpeedRule, LocationRule } from "./RuleTypes";
import Boid from "./Boid";

const rules = {
    acceleration(acceleration: Coordinate): SpeedRule {
        return speed => {
            return Coordinate.add(speed, acceleration);
        }
    },

    // get away if you're getting to close
    separation(boids: Array<Boid>, radius: number, factor: number): BoidRule {
        return (boid) => {

            const acceleration = boids
                .filter((otherBoid: Boid) => otherBoid !== boid)
                .filter((otherBoid: Boid) => Coordinate.dist(boid.location, otherBoid.location) < radius)
                .map((otherBoid: Boid) => {
                    let acceleration = Coordinate.sub(boid.location, otherBoid.location);

                    acceleration = Coordinate.multiplyNumber(acceleration, 1 / Coordinate.dist(boid.location, otherBoid.location));
                    return acceleration;
                })
                .reduce(Coordinate.add, Coordinate.zero());

            return Coordinate.multiplyNumber(acceleration, factor);
        }
    },

    // steer in to the direction of your neighbours
    alignment(boids: Array<Boid>, radius: number, factor: number): BoidRule {
        return (boid) => {
            const acceleration = boids
                .filter((otherBoid: Boid) => otherBoid !== boid)
                .filter((otherBoid: Boid) => Coordinate.dist(boid.location, otherBoid.location) < radius)
                .map((otherBoid: Boid) => Coordinate.sub(otherBoid.speed, boid.speed))
                .reduce(Coordinate.add, Coordinate.zero());

            return Coordinate.multiplyNumber(acceleration, factor);
        }
    },

    // form groups
    cohesion(boids: Array<Boid>, radius: number, factor: number): BoidRule {
        return (boid) => {

            const filteredBoids = boids
                .filter(otherBoid => otherBoid !== boid)
                .filter(otherBoid => Coordinate.dist(boid.location, otherBoid.location) < radius);

            let middle = filteredBoids
                .map(otherBoid => otherBoid.location)
                .reduce(Coordinate.add, Coordinate.zero());

            if (filteredBoids.length > 0) {
                middle = Coordinate.multiplyNumber(middle, 1 / filteredBoids.length);

                const acceleration = Coordinate.sub(middle, boid.location);

                return Coordinate.multiplyNumber(acceleration, factor);
            } else {
                return Coordinate.zero();
            }
        }
    },

    // limit the speed
    maxSpeed(maxSpeed: number): SpeedRule {
        return (speed) => {
            if (Coordinate.size(speed) > maxSpeed)
                return Coordinate.normalize(speed, maxSpeed);

            return speed;
        }
    },

    // limit the speed
    keepInBounds(bounds: Coordinate): LocationRule {
        return (location) => {
            return Coordinate.keepInBounds(location, bounds);
        }
    }
}

export default rules;