const path = require('path');

module.exports = {
    mode: 'development',
    // watch: true,
    entry: "./src/main.ts",
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'out')
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    }
}
